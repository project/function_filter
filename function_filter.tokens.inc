<?php

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;
use Drupal\function_filter\FunctionFilterManager;

/**
 * Implements hook_token_info().
 */
function function_filter_token_info(): array {
  $info = [];

  $info['types']['function'] = [
    'name' => t('Function'),
    'description' => t('Function Filter module tokens.'),
  ];

  $function_filter_manager = \Drupal::service('function_filter.manager'); /** @var FunctionFilterManager $function_filter_manager */
  $functions = $function_filter_manager->getFunctions();
  foreach ($functions as $function_name => $function_info) {
    $info['tokens']['function'][$function_name] = [
      'name' => $function_name,
      'description' => $function_info['function'],
      'dynamic' => TRUE,
    ];
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function function_filter_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];

  if ($type == 'function') {
    $function_filter_manager = \Drupal::service('function_filter.manager'); /** @var FunctionFilterManager $function_filter_manager */

    foreach ($tokens as $name => $original) {
      $replacements[$original] = Markup::create($function_filter_manager->getRenderedFunctionResult($name, $bubbleable_metadata));
    }
  }

  return $replacements;
}
