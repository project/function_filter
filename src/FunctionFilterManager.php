<?php

namespace Drupal\function_filter;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Security\TrustedCallbackInterface;

class FunctionFilterManager implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks(): array {
    return [
      'lazyBuilderCallback',
    ];
  }

  /**
   * Return filter functions.
   */
  public function getFunctions(): array {
    $functions = &drupal_static(__FUNCTION__);

    if ($functions === NULL) {
      $functions = \Drupal::moduleHandler()->invokeAll('filter_functions');
    }

    return $functions;
  }

  /**
   * Return function info.
   */
  public function getFunctionInfoByTokenParam(string $token_param): ?array {
    $token_params = explode(':', $token_param, 2);
    $function_name = str_replace('-', '_', $token_params[0]);
    $function_name = preg_replace('/[^a-z0-9_]/', '', $function_name);
    $functions = $this->getFunctions();

    if (isset($functions[$function_name])) {
      $function_arguments = isset($token_params[1]) ? explode(',', $token_params[1]) : [];
      return $functions[$function_name] + ['arguments' => $function_arguments];
    }

    return NULL;
  }

  /**
   * Return function result build.
   *
   * @return array|string
   */
  public function getFunctionResultBuild(string $token_params, BubbleableMetadata $bubbleable_metadata) {
    $function_info = $this->getFunctionInfoByTokenParam($token_params);

    if ($function_info) {
      // Lazy function
      if (!empty($function_info['lazy'])) {
        $build = [
          '#lazy_builder' => [
            // Callback name
            'function_filter.manager:lazyBuilderCallback',
            // Callback params
            [$token_params],
          ],
          '#create_placeholder' => TRUE,
        ];
        $result = \Drupal::service('renderer')->render($build);
      }
      // Normal function
      else {
        $result = $this->getRenderedFunctionResult($token_params, $bubbleable_metadata);
      }
    }
    else {
      return '[function:' . $token_params . ']';
    }

    return $result;
  }

  /**
   * Return function result.
   */
  public function getRenderedFunctionResult(string $token_params, BubbleableMetadata &$bubbleable_metadata): string {
    $function_info = $this->getFunctionInfoByTokenParam($token_params);

    if (!$function_info) {
      return '[function:' . $token_params . ']';
    }
    if (isset($function_info['file'])) {
      require_once $function_info['file'];
    }

    $result = call_user_func_array($function_info['function'], $function_info['arguments']);

    if (is_array($result)) {
      $result_bubbleable_metadata = BubbleableMetadata::createFromRenderArray($result);
      $bubbleable_metadata = $bubbleable_metadata->merge($result_bubbleable_metadata); // Add #attachments and #cache from $result to $filter_result
      $result = \Drupal::service('renderer')->render($result);
    }
    if (isset($function_info['cache']) && $function_info['cache'] === FALSE) {
      $bubbleable_metadata->setCacheMaxAge(0);
    }

    return $result;
  }

  /**
   * Lazy builder callback.
   * @see \Drupal\function_filter\Plugin\Filter\FunctionFilter::process()
   */
  public function lazyBuilderCallback(string $token_params): array {
    $bubbleable_metadata = BubbleableMetadata::createFromRenderArray([]);
    $result = [
      '#markup' => $this->getRenderedFunctionResult($token_params, $bubbleable_metadata),
    ];
    $bubbleable_metadata->applyTo($result);
    return $result;
  }

}
