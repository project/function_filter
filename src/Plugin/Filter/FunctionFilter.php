<?php

namespace Drupal\function_filter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\function_filter\FunctionFilterManager;

/**
 * @Filter(
 *   id = "function_filter",
 *   title = @Translation("Function filter"),
 *   description = @Translation("Replace tokens [function:*] to function result."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   weight = 20,
 * )
 */
class FunctionFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $filter_result = new FilterProcessResult();
    $function_filter_manager = \Drupal::service('function_filter.manager'); /** @var FunctionFilterManager $function_filter_manager */

    $text = preg_replace_callback('/\[function:(.+?)]/', function ($matches) use ($filter_result, $function_filter_manager) {
      return $function_filter_manager->getFunctionResultBuild($matches[1], $filter_result);
    }, $text);

    $filter_result->setProcessedText($text);

    return $filter_result;
  }

}
