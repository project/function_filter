<?php

use Drupal\block\Entity\Block;
use Drupal\block_content\Entity\BlockContent;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;

/**
 * Return current date.
 */
function function_filter__current_date(string $format = 'r'): string {
  return date($format);
}

/**
 * Return contact form.
 */
function function_filter__contact_form(string $contact_form_id): array {
  $contact_message = \Drupal::entityTypeManager()
    ->getStorage('contact_message')
    ->create(['contact_form' => $contact_form_id]);

  $form = \Drupal::service('entity.form_builder')->getForm($contact_message);
  $form['#cache']['contexts'][] = 'user.permissions';

  return $form;
}

/**
 * Return views.
 */
function function_filter__views(string $view_id, string $display_name = 'default', string $arguments = ''): array {
  $arguments = array_diff(explode('/', $arguments), ['']);
  return views_embed_view($view_id, $display_name, ...$arguments);
}

/**
 * Return menu.
 */
function function_filter__menu(string $menu_name, string $root = '', string $add_cache_context_by_active_trail = '1'): array {
  $menu_tree_builder = \Drupal::menuTree();

  $menu_tree_parameters = $menu_tree_builder->getCurrentRouteMenuTreeParameters($menu_name);
  $menu_tree_parameters->setMinDepth(1);

  if ($root) {
    $menu_tree_parameters->setRoot($root)->excludeRoot();
  }

  $menu_tree = $menu_tree_builder->load($menu_name, $menu_tree_parameters);
  $menu_tree = $menu_tree_builder->transform($menu_tree, [
    ['callable' => 'menu.default_tree_manipulators:checkAccess'],
    ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
  ]);

  $menu_tree_build = $menu_tree_builder->build($menu_tree);

  if ($add_cache_context_by_active_trail) {
    $menu_tree_build['#cache']['contexts'][] = 'route.menu_active_trails:' . $menu_name;
  }

  return $menu_tree_build;
}

/**
 * Return site mail.
 */
function function_filter__site_mail(): string {
  return \Drupal::config('system.site')->get('mail');
}

/**
 * Return site url.
 */
function function_filter__site_url(string $absolute = '0'): string {
  $url_options['absolute'] = $absolute;
  return Url::fromRoute('<front>', [], $url_options)->toString();
}

/**
 * Return configured block.
 */
function function_filter__configured_block(string $block_config_id): array {
  $block = Block::load($block_config_id);
  return \Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
}

/**
 * Return content block.
 */
function function_filter__content_block(string $content_block_id, string $wrapper = '1', string $label = ''): array {
  if ($content_block = BlockContent::load($content_block_id)) {
    $content_block_build = function_filter__block_plugin('block_content:' . $content_block->uuid(), $wrapper, $label);

    $content_block_build['#contextual_links'] = [
      'block_content' => [
        'route_parameters' => ['block_content' => $content_block_id],
      ],
    ];
    $content_block_build['#id'] = $content_block->get('type')->target_id . '-block-' . $content_block_id;

    return $content_block_build;
  }

  return [
    '#markup' => t('Content block "@id" not exists.', ['@id' => $content_block_id]),
  ];
}

/**
 * Return block plugin.
 */
function function_filter__block_plugin(string $block_plugin_id, string $wrapper = '1', string $label = ''): array {
  $block_manager = \Drupal::service('plugin.manager.block');
  $block_plugin = $block_manager->createInstance($block_plugin_id, []); /** @var BlockPluginInterface $block_plugin */
  $access_result = $block_plugin->access(\Drupal::currentUser());

  if (
    (is_object($access_result) && $access_result->isAllowed()) ||
    (is_bool($access_result) && $access_result)
  ) {
    if ($block_content = $block_plugin->build()) {
      if ($wrapper) {
        $block_build = [
          '#theme' => 'block',
          '#configuration' => [
            'label' => $label,
            'label_display' => BlockPluginInterface::BLOCK_LABEL_VISIBLE,
          ] + $block_plugin->getConfiguration(),
          '#plugin_id' => $block_plugin->getPluginId(),
          '#base_plugin_id' => $block_plugin->getBaseId(),
          '#derivative_plugin_id' => $block_plugin->getDerivativeId(),
          'content' => $block_content,
        ];

        BubbleableMetadata::createFromRenderArray($block_build)
          ->merge(BubbleableMetadata::createFromRenderArray($block_content))
          ->merge(BubbleableMetadata::createFromObject($block_plugin))
          ->applyTo($block_build);

        return $block_build;
      }

      return $block_content;
    }
  }

  return [];
}
